
from subprocess import call

# Variable
db = "zabbix"
sqlUser = "zabbix"
sqlPass = "zabbix@pwd"
sqlRootPass = "root@pwd"
dbServerHost = "mysql-server"
portNginx = "80"
zabbixVer = "alpine-6.4-latest"
netWork = "zabbix-net"
restart = "unless-stopped"

#||||||||||| Install Docker Engine ||||||||||||
print('\n======= # 1 INSTALL DOCKER ENGINE =======\n')
print('\n======= # 1.1 Set up the repository =======\n')
# 1.1.1---
call('sudo apt-get update', shell=True)
# 
gnupg = call('sudo apt-get install ca-certificates curl gnupg', shell=True)
# 
if gnupg != 0:
    print('\nError: Install gnupg INCOMPLETE !!!\n')
    quit()
# 1.1.2---
call('sudo install -m 0755 -d /etc/apt/keyrings', shell=True)
call('sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg', shell=True)
call('sudo chmod a+r /etc/apt/keyrings/docker.gpg', shell=True)
# 1.1.3---
call('sudo echo \
        "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
        "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null', 
    shell=True)
# 
print('\n======= # 1.2 Install Docker, docker-compose =======\n')
call('sudo apt-get update', shell=True)
call('sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y', shell=True)
# 
print('\n======= # 2 INSTALL ZABBIX SERVER =======\n')

# 1. Create network dedicated for Zabbix component containers:
print('\n======= # 2.1 Create network dedicated =======\n')
# 
r1 = call('sudo docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 ' + netWork + '', shell=True)

# 
if r1 != 0:
    print('\nError: Create network dedicated for Zabbix component containers INCOMPLETE !!!\n')
    quit()
# 
# 2. Start empty MySQL server instance
print('\n======= # 2.2 Start empty MySQL server =======\n')
# 
r2 = call(
        'sudo docker run --name mysql-server -t \
            -e MYSQL_DATABASE=' + db +' \
            -e MYSQL_USER=' + sqlUser +' \
            -e MYSQL_PASSWORD=' + sqlPass + '\
            -e MYSQL_ROOT_PASSWORD=' + sqlRootPass + ' \
            --network=' + netWork + ' \
            --restart ' + restart + ' \
            -d mysql:8.0-oracle \
            --character-set-server=utf8 --collation-server=utf8_bin \
            --default-authentication-plugin='+sqlPass+'',
    shell=True)
if r2 != 0:
    print('\nError: Start empty MySQL server instance INCOMPLETE !!!\n')
    quit()
# 
# 3. Start Zabbix Java gateway instance
print('\n======= # 2.3 Start Zabbix Java gateway =======\n')
# 
r3 = call(
        'sudo docker run --name zabbix-java-gateway -t \
            --network=' + netWork + ' \
            --restart '+ restart + ' \
            -d zabbix/zabbix-java-gateway:' + zabbixVer + '',
    shell=True)
if r3 != 0:
    print('\nError: Start Zabbix Java gateway instance INCOMPLETE !!!\n')
    quit()
# 
# 4. Start Zabbix server instance and link the instance with created MySQL server instance
print('\n======= # 2.4 Start Zabbix server and link with created MySQL server =======\n')
# 
r4 = call(
        'sudo docker run --name zabbix-server-mysql -t \
            -e DB_SERVER_HOST=' + dbServerHost + ' \
            -e MYSQL_DATABASE=' + db +' \
            -e MYSQL_USER=' + sqlUser +' \
            -e MYSQL_PASSWORD=' + sqlPass + '\
            -e MYSQL_ROOT_PASSWORD=' + sqlRootPass + ' \
            -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
            --network=' + netWork + ' \
            -p 10051:10051 \
            --restart ' + restart + ' \
            -d zabbix/zabbix-server-mysql:'+ zabbixVer +'',
    shell=True)
# 
if r4 != 0:
    print('\nError: Start Zabbix server and link created MySQL server INCOMPLETE !!!\n')
    quit()
# 
# 5. Start Zabbix web interface and link the instance with created MySQL server and Zabbix server instances
print('\n======= # 2.5 Start Zabbix web interface link with created MySQL server and Zabbix server =======\n')

r5 = call(
        'sudo docker run --name zabbix-web-nginx-mysql -t \
            -e ZBX_SERVER_HOST="zabbix-server-mysql" \
            -e DB_SERVER_HOST=' + dbServerHost + ' \
            -e MYSQL_DATABASE=' + db + ' \
            -e MYSQL_USER=' + sqlUser + ' \
            -e MYSQL_PASSWORD=' + sqlPass + '\
            -e MYSQL_ROOT_PASSWORD=' + sqlRootPass + ' \
            --network=' + netWork + ' \
            -p' + portNginx +':8080 \
            --restart ' + restart + ' \
            -d zabbix/zabbix-web-nginx-mysql:' + zabbixVer +'',
    shell=True)
# 
if r5 != 0:
    print('\nError: Start Zabbix web interface and link with MySQL server and Zabbix server INCOMPLETE !!!\n')
    quit()
# 
call('systemctl staus zabbix-server',shell=True)
# 
print('\n||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
print('\n||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
print('\n|||||||||||||||||||||||| HAPPYYY 凸(¬‿¬)凸 SUCCESSFULL |||||||||||||||||||||||')
print('\n||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
print('\n||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
# 
print('\n***NOTE: Default Account to login Zabbix Server: Admin/zabbix\n')
