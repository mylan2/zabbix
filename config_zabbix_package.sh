#!/bin/bash

# Variables
source_url="https://repo.zabbix.com/zabbix/6.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.4-1+ubuntu22.04_all.deb"
source_file="zabbix-release_6.4-1+ubuntu22.04_all.deb"
zabbix_services="-y zabbix-server-mysql zabbix-agent zabbix-apache-conf zabbix-sql-scripts zabbix-frontend-php"
mysql_server="-y mysql-server"
mysql_conf="/etc/mysql/mysql.conf.d/mysqld.cnf"
default_root_pass="password"
change_root_pass="ALTER USER 'root@localhost' IDENTIFIED BY '$default_root_pass';"
zabbix_dbname="zabbix"
zabbix_dbuser="zabbix"
zabbix_conf="/etc/zabbix/zabbix_server.conf"
zabbix_server_agent_processes="zabbix-server zabbix-agent apache2"
# Zabbix Agent
zabbix_services_agent="-y zabbix-agent"
zabbix_agent_conf="/etc/zabbix/zabbix_agentd.conf"
#  
cr=`echo $'\n.'`
cr=${cr%.}
# Function
comfirm() {
      while true; do
            read -p "$1 [Y]es or [N]o): " reply
            case "${reply:-Y}" in
                  [Yy]* ) return 0;;
                  [Nn]* ) return 1;;
                  * ) echo "Error: Please answer yes or no !!!";;
            esac
      done
} 
# Echo newline
echon() { 
      echo -e "$1\n"
}
# Get ethernet card
eth() {
      ip r | grep default | awk '/default/ {print $5}'
}
# get IP addresss
ipar() {
      ip -4 -o addr show $(eth) | awk '{print $4}' | cut -d "/" -f 1
}
#> End line
nechon() {
      echo "$cr$1$cr"
}
# Install Zabbix Repository 
zabbix_install() { 
      echon "#1.1 Download source package"
      sudo wget $source_url
      echon "#1.2. Instal source package"
      sudo dpkg -i $source_file
      echon "#1.3. Update ..."
      sudo apt update
      echon "#1.4. Install Zabbix services"
      sudo apt install $1
}

# Check run as root
if [ "$EUID" -ne 0 ]; then
      nechon "Error: This script must be run as root !!!"
      exit
fi
# Check Zabbix
if which zabbix >/dev/null; then 
      nechon "||||||||||| ZABBIX IS ALREADY INSTALLED |||||||||||"
      exit
fi
# 
# Choose option 
zabbix_server='[1]. Install Zabbix Server'
zabbix_agent='[2]. Install Zabbix Agent'
read -p "${cr}Please choose option to install: ${cr}$zabbix_server ${cr}$zabbix_agent${cr}Your choice: "  option
      case "${option}" in
            [1] ) 
            # Install Zabbix Server
            nechon "#1. Install Zabbix Server"
            zabbix_install "$zabbix_services"
            # Create initial database
            if dpkg --get-selections | grep mysql-server; then 
                  nechon "#2 MySql-server is installed"
            else 
                  nechon "====== #2 Install MySql ======"
                  nechon "#2.1. MySql installing in process"
                  sudo apt install $mysql_server
                  sudo systemctl start mysql.service
                  sudo systemctl enable mysql.service
                  nechon "#2.2. Set IP for MySql Server"
                  nechon "Note: Card found: $(eth)"
                  # Get Ip address
                  nechon "Note: IP found: $(ipar)"
                  sudo sed -i "s/bind-address.*/bind-address = $(ipar)/" $mysql_conf
                  sudo systemctl restart mysql.service
            fi
            nechon "====== #3. Create Initial Zabbix Database ======"
            if comfirm "Do you want to change root password ? "; then
                  read -sp "Enter your password: " password
                  default_root_pass=$password
                  sudo mysql -u root -e $change_root_pass
                  sudo mysql -u root -e "FLUSH PRIVILEGES;"
                  nechon $'\n'
            fi
            #  
            nechon "#3.1. Create Database Zabbix"
            sudo mysql -u root -p$default_root_pass -e "create database $zabbix_dbname character set utf8mb4 collate utf8mb4_bin;"
            nechon "#3.2. Create User Zabbix"
            sudo mysql -u root -p$default_root_pass -e "create user $zabbix_dbuser@localhost identified by '$default_root_pass';"
            nechon "#3.3. grant all privileges for user zabbix"
            sudo mysql -u root -p$default_root_pass -e "grant all privileges on $zabbix_dbname.* to $zabbix_dbuser@localhost;"
            nechon "#3.4. Enable global log_bin_trust_function_creators"
            sudo mysql -u root -p$default_root_pass -e "set global log_bin_trust_function_creators = 1;"
            nechon "#3.5. Import initial schema and data"
            sudo zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql --default-character-set=utf8mb4 -u$zabbix_dbuser -p$default_root_pass $zabbix_dbname
            # Disable log_bin_trust_function_creators option after importing database schema.
            nechon "#2.6. Disable global log_bin_trust_function_creators"
            sudo mysql -u root -p$default_root_pass -e "set global log_bin_trust_function_creators = 0;"
            nechon "#2.7. Configure the database for Zabbix server"
            
            sudo sed -i "s/# DBPassword=/DBPassword=$default_root_pass/" $zabbix_conf
            sudo sed -i "s/DBName=.*/DBName=$zabbix_dbname/" $zabbix_conf
            sudo sed -i "s/DBUser=.*/DBUser=$zabbix_dbname/" $zabbix_conf 
            nechon "#3. Start Zabbix server and agent processes"
            if sudo ufw status | grep -qw active; then
            nechon "#3.1. Configure firewall"
                  sudo ufw allow 10050/tcp
                  sudo ufw allow 10051/tcp
                  sudo ufw allow 80/tcp
                  sudo ufw reload
            fi
            sudo systemctl restart $zabbix_server_agent_processes 
            sudo systemctl enable $zabbix_server_agent_processes
            ;;
            [2] ) 
            # Install Zabbix Agent 
            zabbix_install "$zabbix_services_agent"
            # 
            nechon "#1.5. Configure Zabbix agent to be able to communicate with the Zabbix server"
            read -p "Enter Zabbix Server IP: " ip_zabbix_server
            read -p "Enter Host Name for Zabbix Agent: " hostname_zabbix_agent
            # Config /etc/zabbix/zabbix_agentd.conf
            sudo sed -i "s/Server=.*/Server=$ip_zabbix_server/" $zabbix_agent_conf
            sudo sed -i "s/ServerActive=.*/ServerActive=$ip_zabbix_server/" $zabbix_agent_conf
            sudo sed -i "s/Hostname=.*/Hostname=$hostname_zabbix_agent/" $zabbix_agent_conf
            nechon "#1.6. Start and enable Zabbix Agent service"
            systemctl restart zabbix-agent
            systemctl enable --now zabbix-agent
            systemctl status zabbix-agent
            # Enable firewall
            if sudo ufw status | grep -qw active; then
                  echon "#2.7. Configure firewall"
                  ufw allow from $ip_zabbix_server to any port 10050 proto tcp comment "Allow Zabbix Server"
            fi
            ;;
             * ) echo "Error: Please answer 1 or 2 !!!";;
      esac
# 


